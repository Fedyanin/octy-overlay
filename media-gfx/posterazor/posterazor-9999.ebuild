# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 desktop

DESCRIPTION="PosteRazor cuts raster images into multipage PDF documents."
HOMEPAGE="http://posterazor.sourceforge.net/"
LICENSE="GPL-2"
SLOT="0"
EGIT_REPO_URI="https://github.com/aportale/posterazor.git"
SRC_URI=""

KEYWORDS="amd64 x86"
IUSE=""

PATCHES=( "${FILESDIR}/2020-qpaintpath.patch" )

#FIXME: add freeimage use flag"
RDEPEND="dev-qt/qtgui"
DEPEND="${RDEPEND}"


src_configure() {
	cd src
	qmake -o Makefile posterazor.pro
}

src_compile() {
	cd src
	emake
}

src_install() {
	exeinto /usr/bin
	doexe ${S}/src/PosteRazor
	dodoc ${S}/CHANGES ${S}/LICENSE ${S}/README
	doicon ${S}/src/posterazor.png
	make_desktop_entry PosteRazor Posterazor posterazor

}
