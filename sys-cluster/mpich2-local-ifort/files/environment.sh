#!/bin/sh

export PATH=/opt/MPICHDIR/bin:$PATH
export MANPATH=/opt/MPICHDIR/share/man:${MANPATH}
export FPATH=/opt/MPICHDIR/include:${FPATH}
export LIBRARY_PATH=/opt/MPICHDIR/lib:${LIBRARY_PATH}
