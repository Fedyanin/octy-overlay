# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-cluster/mpich2/mpich2-1.5.ebuild,v 1.1 2012/11/02 04:37:25 jsbronder Exp $

EAPI=7

MY_PV=${PV/_/}
MY_PN=${PN/-local-ifort/}
DESCRIPTION="A high performance and portable MPI implementation"
HOMEPAGE="http://www.mcs.anl.gov/research/projects/mpich2/index.php"
SRC_URI="https://www.mpich.org/static/downloads/${MY_PV}/${MY_PN}-${MY_PV}.tar.gz"

SLOT="0"
LICENSE="as-is"
KEYWORDS="~amd64 ~hppa ~ppc ~ppc64 ~x86"
IUSE=""

COMMON_DEPEND=""

DEPEND="${COMMON_DEPEND}
    dev-lang/ifc
	dev-lang/perl
	sys-devel/libtool"

S="${WORKDIR}"/${MY_PN}-${MY_PV}

src_prepare() {
	# Using MPICH2LIB_LDFLAGS doesn't seem to full work.
	eapply "${FILESDIR}/${P}-fno-common.patch"
	eapply_user
	sed -i 's| *@WRAPPER_LDFLAGS@ *||' \
		src/packaging/pkgconfig/mpich2.pc.in \
		src/env/*.in \
		|| die
}

src_configure() {
	local c="--enable-shared"

	export FCFLAGS="-O2 -xHost"
	export FC="ifort"
	export MPICH2LIB_CFLAGS=${CFLAGS}
	export MPICH2LIB_CPPFLAGS=${CPPFLAGS}
	export MPICH2LIB_CXXFLAGS=${CXXFLAGS}
	export MPICH2LIB_FFLAGS=${FFLAGS}
	export MPICH2LIB_FCFLAGS=${FCFLAGS}
	export MPICH2LIB_LDFLAGS=${LDFLAGS}
	unset F90 F77 F90FLAGS
	unset CFLAGS CPPFLAGS CXXFLAGS FFLAGS LDFLAGS

	c="${c} --prefix=/opt/${PN}-${PV}"
	./configure ${c} \
		--with-pm=hydra \
		--enable-mpe \
		--enable-fc \
		--enable-fast \
		--enable-smpcoll \
		--enable-versioning 
}

src_install() {
    emake DESTDIR="${D}" install || die "Install failed"
	sed -e "s:MPICHDIR:${PN}-${PV}:g" ${FILESDIR}/environment.sh > ${D}/opt/${PN}-${PV}/environment.sh
}

