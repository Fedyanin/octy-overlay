# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

FORTRAN_NEEDED=fortran

inherit cuda flag-o-matic fortran-2 java-pkg-opt-2 toolchain-funcs 

MY_P=${P/-local-ifort/}
S=${WORKDIR}/${MY_P}
export FCFLAGS="-O2 -xHost"
export FC="ifort"
SLOT=${PV}


DESCRIPTION="A high-performance message passing library (MPI)"
HOMEPAGE="http://www.open-mpi.org"
SRC_URI="http://www.open-mpi.org/software/ompi/v$(ver_cut 1-2)/downloads/${MY_P}.tar.bz2"
LICENSE="BSD"
SLOT="0"
KEYWORDS="~alpha ~amd64 arm ~ppc ~ppc64 ~x86 ~amd64-fbsd ~x86-fbsd ~amd64-linux"
IUSE="cma cxx elibc_FreeBSD fortran heterogeneous ipv6 mpi-threads numa romio threads"

# dev-util/nvidia-cuda-toolkit is always multilib
CDEPEND="
	!sys-cluster/mpich
	!sys-cluster/mpich2
	!sys-cluster/nullmpi
	!sys-cluster/mpiexec
	>=dev-libs/libevent-2.0.22[${MULTILIB_USEDEP}]
	dev-libs/libltdl:0[${MULTILIB_USEDEP}]
	>=sys-apps/hwloc-1.11.2[${MULTILIB_USEDEP},numa?]
	>=sys-libs/zlib-1.2.8-r1[${MULTILIB_USEDEP}]
	elibc_FreeBSD? ( dev-libs/libexecinfo )"

pkg_setup() {
	fortran-2_pkg_setup
	java-pkg-opt-2_pkg_setup

	elog
	elog "OpenMPI has an overwhelming count of configuration options."
	elog "Don't forget the EXTRA_ECONF environment variable can let you"
	elog "specify configure options if you find them necessary."
	elog
}

src_prepare() {
	default

	# Necessary for scalibility, see
	# http://www.open-mpi.org/community/lists/users/2008/09/6514.php
	if use threads; then
		echo 'oob_tcp_listen_mode = listen_thread' \
			>> opal/etc/openmpi-mca-params.conf || die
	fi
}

src_configure() {
	if use java; then
		# We must always build with the right -source and -target
		# flags. Passing flags to javac isn't explicitly supported here
		# but we can cheat by overriding the configure test for javac.
		export ac_cv_path_JAVAC="$(java-pkg_get-javac) $(java-pkg_javac-args)"
	fi

    ./configure \
        $(use_enable cxx mpi-cxx) \
        $(use_enable romio io-romio) \
        $(use_enable heterogeneous) \
        $(use_enable ipv6) \
        --prefix=/opt/${PN}-${PV} \
        --docdir=/opt/${PN}-${PV}/doc

}

src_install () {
	emake DESTDIR="${D}" install || die "make install failed"
	# From USE=vt see #359917
	rm "${ED}"/usr/share/libtool &> /dev/null
	DD="${ED}/opt/${PN}-${PV}/doc"
	for i in README AUTHORS NEWS VERSION; do
	    bzip2 -c $i > $DD/$i.bz2
	done
}
