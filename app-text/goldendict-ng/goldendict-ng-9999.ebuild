# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PLOCALES="ar_SA ay_WI be_BY be_BY@latin bg_BG cs_CZ de_DE el_GR eo_EO es_AR es_BO es_ES fa_IR fi_FI fr_FR hi_IN ie_001 it_IT ja_JP jb_JB ko_KR lt_LT mk_MK nl_NL pl_PL pt_BR qt_es qt_it qt_lt qu_WI ru_RU sk_SK sq_AL sr_SR sv_SE tg_TJ tk_TM tr_TR uk_UA vi_VN zh_CN zh_TW"

inherit plocale desktop git-r3 qmake-utils

KEYWORDS="~x86 ~amd64"

DESCRIPTION="Feature-rich dictionary lookup program"
HOMEPAGE="https://github.com/xiaoyifang/goldendict-ng/"
EGIT_REPO_URI="https://github.com/xiaoyifang/${PN}.git"
EGIT_SUBMODULES=()

PNP=goldendict

LICENSE="GPL-3"
SLOT="0"
IUSE="debug ffmpeg"

RDEPEND="
	media-libs/libvorbis
	sys-libs/zlib
	>=app-text/hunspell-1.2:=
	x11-libs/libX11
	x11-libs/libXtst
	dev-libs/lzo
	app-arch/bzip2
	dev-libs/eb
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtsvg:5
	dev-qt/qtx11extras:5
	dev-qt/qtwebengine:5
	dev-qt/qtx11extras:5
	dev-qt/qtwebchannel
	dev-qt/qtxml:5
	dev-qt/qtmultimedia:5
	media-video/ffmpeg:0=
"
DEPEND="${RDEPEND}"
BDEPEND="
	dev-qt/linguist-tools:5
	virtual/pkgconfig
"
src_prepare() {
	default

	# disable git
	sed -i -e '/git describe/s/^/#/' ${PNP}.pro || die

	# fix installation path
	sed -i -e '/PREFIX = /s:/usr/local:/usr:' ${PNP}.pro || die

	# add trailing semicolon

	echo "QMAKE_CXXFLAGS_RELEASE = $CXXFLAGS" >> ${PNP}.pro
	echo "QMAKE_CFLAGS_RELEASE = $CFLAGS" >> ${PNP}.pro

	local loc_dir="${S}/locale"
	plocale_find_changes "${loc_dir}" "" ".ts"
	rm_loc() {
		rm -vf "locale/${1}.ts" || die
		sed -i "/${1}.ts/d" ${PNP}.pro || die
	}
	plocale_for_each_disabled_locale rm_loc
}


src_configure() {
	eqmake5
}

install_locale() {
	insinto /usr/share/${PNP}/locale
	ebegin "Installing locale ${1}"
		if [[ -e "${S}"/locale/${1}.qm ]] ; then
			doins "${S}"/locale/${1}.qm
		fi
	eend $? || die "failed to install $1 locale"
}

src_install() {
	dobin ${PNP}
	domenu redist/org.${PNP}.GoldenDict.desktop
	doicon redist/icons/${PNP}.png

#	insinto /usr/share/${PNP}/help
#	doins help/gdhelp_en.qch
	plocale_for_each_locale install_locale
}
