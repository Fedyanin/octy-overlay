# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI="2"


SRC_FILE="x86? ( xd2006_linux32.tar.gz ) amd64? ( xd2006_linux64.tar.gz )"

DESCRIPTION="SHELXTL program suite"
HOMEPAGE="http://xd.chem.buffalo.edu/"
SRC_URI="$SRC_FILE"
RESTRICT="fetch"

XD_ROOT="/opt/xd"

SLOT="0"
KEYWORDS="~x86 ~amd64"

RDEPEND=""
DEPEND="${RDEPEND} app-shells/tcsh"

S=${WORKDIR}/xd2006

pkg_nofetch() {
	elog "Please download ${SRC_FILE} from $HOMEPAGE"
	elog "and place it to ${DISTDIR}"
}

src_prepare() {
    cd "${S}"
	tcl_v="$(best_version dev-lang/tcl | cut -d- -f3 | cut -d. -f1,2)"
	tk_v="$(best_version dev-lang/tk | cut -d- -f3 | cut -d. -f1,2)"
	sm=${tk_v/./}
	ewarn $tcl_version $tk_version
    sed -i -e "s/ltk8.3/ltk${tk_v}/g" -e "s/ltcl8.3/ltcl${tcl_v}/g" \
	    -e "s:^XD_PREFIX=.*:XD_PREFIX=${XD_ROOT}:g" \
	    -e "s/DXD_TK_VERSION=[^ ]*/DXD_TK_VERSION=$sm/g" \
		-e "s/DRIVER_FLAGS=/DRIVER_FLAGS=-DUSE_INTERP_RESULT -DUSE_INTERP_ERRORLINE /g" mk.config
	cd ports
	mv mk.gfortran mk.linux
	cd ${S}/xdgraph/src
	sed -i -e 's/^default:.*/default: linux/g' Makefile
}

src_compile() {
	cd ${S}/xdgraph/src
	make
}


src_install() {
	BINDIR="$D/opt/bin"
	mkdir -p $BINDIR
	INSDIR="$D/opt/xd"
	mkdir -p $INSDIR

	for i in bin lib; do
		cp -r $S/$i $INSDIR
	done
	mkdir $INSDIR/xdgraph
	cp -r $S/xdgraph/tcl $INSDIR/xdgraph
	cp $S/xdgraph/src/obj.linux/xdgraph $INSDIR/bin


	cd $INSDIR/bin;
	for i in `ls * | grep -v "^xd$"` ; do
		dosym /opt/xd/bin/$i opt/bin/$i
	done
	cd -


	echo "XD_LIBDIR=${XD_ROOT}/lib" > "${T}"/env.d
	echo "XD_BINDIR=${XD_ROOT}/bin" >> "${T}"/env.d
	echo "XD_TCLDIR=${XD_ROOT}/lib/xdgraph" >> "${T}"/env.d
	echo "XD_LIBDIR=${XD_ROOT}/lib/xd" >> "${T}"/env.d
	echo "XD_DATADIR=${XD_ROOT}/lib/xd" >> "${T}"/env.d
	newenvd "${T}"/env.d 50xd || die
}

pkg_postinst(){
	cd /opt/xd/bin
	./xdkeyinfo >/dev/null
	mv hardware.info ..
	cd ..
	HWI=`cat hardware.info | iconv -c -f utf-8 -t cp1250`
	elog "File 'hardware info' is now in '/opt/xd' directory"
	elog "Goto http://www.chem.gla.ac.uk/~louis/software/xdlicense.html"
	elog "And use 'h81-0V1-68K-B8p', Mikhail Antipin as identification"
	elog "hardware info string is '$HWI'"
}
