# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI="2"
inherit eutils

DESCRIPTION="Semiempirical quantum chemistry package"
HOMEPAGE="http://openmopac.net"
SRC_URI="ftp://10.5.0.60/overfiles/MOPAC2009_for_Linux.zip"
RESTRICT="nomirror"


LICENSE="MOPAC"
LICENSE_NUMBER="8219631a16196409"
SLOT="0"
KEYWORDS="x86"
DEPEND="${RDEPEND}"

S=${WORKDIR}

src_unpack() {
	cd ${S}
	unpack ${A}
	chmod u+x MOPAC2009.exe
	ewarn "Will use the license number obtained by I. Fedyanin"
	ewarn "Please check this and change if not desirable"
	echo -ne "\nYes\n" | MOPAC_LICENSE=${S} ./MOPAC2009.exe ${LICENSE_NUMBER} > /dev/null
}

src_compile() {
	cd ${S}
	echo -ne "#!/bin/sh\n/opt/mopac/MOPAC2009.exe \$@\n" > mopac
}

src_install() {
	exeinto /opt/mopac
	doexe MOPAC2009.exe
	insinto /opt/mopac
	doins password_for_mopac2009
	exeinto /opt/bin
	doexe mopac
}

