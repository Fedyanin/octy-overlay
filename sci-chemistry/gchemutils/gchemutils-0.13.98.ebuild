# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sci-chemistry/gchemutils/gchemutils-0.13.1.ebuild,v 1.4 2011/11/01 12:24:48 pacho Exp $

EAPI=4
inherit gnome2 multilib nsplugins toolchain-funcs

MY_P=gnome-chemistry-utils-${PV}

DESCRIPTION="GTK widgets and some C++ classes related to chemistry"
HOMEPAGE="http://www.nongnu.org/gchemutils/"
SRC_URI="mirror://nongnu/${PN}/${PV%.*}/${MY_P}.tar.xz"

LICENSE="GPL-2 FDL-1.3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="nls nsplugin"

CDEPEND="app-office/gnumeric
	>=dev-libs/glib-2.26
	dev-libs/libxml2
	>=gnome-extra/libgsf-1.14.9
	sci-chemistry/chemical-mime-data
	sci-chemistry/bodr
	sci-chemistry/openbabel
	virtual/opengl
	x11-libs/cairo
	>=x11-libs/gdk-pixbuf-2.22
	>=x11-libs/goffice-0.9.0
	x11-libs/gtk+:3
	x11-libs/libX11
	x11-misc/shared-mime-info"
RDEPEND="${CDEPEND}
	gnome-extra/yelp" #271998
DEPEND="${CDEPEND}
	app-text/gnome-doc-utils
	dev-libs/libxslt
	dev-util/intltool
	dev-util/pkgconfig
	sys-devel/gettext
	nsplugin? ( net-misc/npapi-sdk )"

S=${WORKDIR}/${MY_P}

pkg_setup() {
	G2CONF="${G2CONF}
		--disable-scrollkeeper
		--disable-schemas-install
		--disable-update-databases
		$(use_enable nsplugin mozilla-plugin)"

	if use nsplugin; then
		G2CONF="${G2CONF} --with-mozilla-libdir=/usr/$(get_libdir)/${PLUGINS_DIR}"
	fi

	DOCS="AUTHORS ChangeLog NEWS README TODO"
}

src_prepare() {
	sed -i -e 's:-DG.*_DISABLE_DEPRECATED::' configure || die #388509
	gnome2_src_prepare
}

src_configure() {
	if use nsplugin; then
		export MOZILLA_CFLAGS="$($tc-getPKG_CONFIG) --cflags npapi-sdk)"
		export MOZILLA_LIBS=""
	fi

	gnome2_src_configure
}

src_test() { :; } #342743

src_install() {
	gnome2_src_install
	find "${ED}" -name '*.la' -exec rm -f {} +
}
