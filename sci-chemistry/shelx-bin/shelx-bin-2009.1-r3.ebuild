# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI="2"
# changes: xprep corrected

SRC_FILE_MAIN="shelxtl-2009.9-1.i386.rpm"
SRC_FILE_ADD1="xp-5.1.exe.gz"
SRC_FILE_SHELXL="shelxl-2014-09-17.bz2"
SRC_FILE_SHELXD="shelxd-2013-04-20.bz2"
SRC_FILE_SHELXT="shelxt-2014-07-24.bz2"
SRC_FILE="$SRC_FILE_MAIN xp_wine? ( $SRC_FILE_ADD1 )"
SRC_FILE="${SRC_FILE} shelxl? ( $SRC_FILE_SHELXL )"
SRC_FILE="${SRC_FILE} shelxd? ( $SRC_FILE_SHELXD )"
SRC_FILE="${SRC_FILE} shelxt? ( $SRC_FILE_SHELXT )"

DESCRIPTION="SHELXTL program suite"
HOMEPAGE="http://www.brukersupport.com/SoftwarePreview.aspx"
SRC_URI="$SRC_FILE"

SLOT="0"
KEYWORDS="~x86 ~amd64"
RESTRICT="fetch"
IUSE="+xp_wine -xshell +xpx +shelxl +shelxd +shelxt"

RDEPEND="x11-terms/rxvt-unicode
         xp_wine? ( app-emulation/wine )"
DEPEND="${RDEPEND} app-arch/rpm2targz"

S=${WORKDIR}/${SRC_FILE_MAIN%.rpm}/usr/local

pkg_nofetch() {
	elog "Please download ${SRC_FILE_MAIN} from $HOMEPAGE"
	elog "and place it to ${DISTDIR}"
	use xp_wine && elog "File ${SRC_FILE_ADD1} should be obtained elsewhere"
}

src_unpack()
{
	rpm2tgz -O $DISTDIR/$SRC_FILE_MAIN | tar xzf -
	cd $S/bin
	mv xprep xprep.x
	echo -ne "#!/bin/sh\n\nurxvt -bg white -cd \`pwd\` -e /opt/shelx/bin/xprep.x \$@ &\n" > xprep
	if use xp_wine; then
		gunzip -c $DISTDIR/$SRC_FILE_ADD1 > xp-5.1.exe
		echo -ne "#!/bin/sh\n\nwine /opt/shelx/bin/xp-5.1.exe \$@ 2>/dev/null &\n" > xp
	fi
	if use shelxl; then
		bunzip2 -c $DISTDIR/$SRC_FILE_SHELXL > shelxl
	fi
	if use shelxd; then
		bunzip2 -c $DISTDIR/$SRC_FILE_SHELXD > shelxd
	fi
	if use shelxt; then
		bunzip2 -c $DISTDIR/$SRC_FILE_SHELXT > shelxt
	fi
	cd ..
	sed -i -e "s%/usr/local/%/opt/shelx/%" bruker/sxtl.ini
}


src_install() {
	mkdir -p $D/opt/bin
	INSDIR=$D/opt/shelx
	mkdir -p $INSDIR

	PROGLIST="cell_now twinabs xcif xl xpro sadabs xc xe xm xs xwat xprep"
	EXELIST="$PROGLIST xprep.x"

	if use xp_wine; then
		PROGLIST="$PROGLIST xp"
		EXELIST="$EXELIST xp xp-5.1.exe"
	fi
	
	if use xpx; then
		PROGLIST="$PROGLIST xpx"
		EXELIST="$EXELIST xpx"
	fi
	
	for k in shelxl shelxd shelxt; do
		if use $k; then
			PROGLIST="$PROGLIST $k"
			EXELIST="$EXELIST $k"
		fi
	done

	exeinto opt/shelx/bin
	for i in ${EXELIST}; do
		doexe $S/bin/$i;
	done

	insinto opt/shelx/bin
	for i in xcif.ang xcif.def xcif.ger xcif.met xcif.rta xcif.rtm; do
		doins $S/bin/$i;
	done

	for i in ${PROGLIST}; do
		dosym /opt/shelx/bin/$i opt/bin/$i
	done

	cp -R $S/bruker $INSDIR

	echo "SXTL=\"/opt/shelx/bruker/sxtl.ini\"" > "${T}"/env.d
	newenvd "${T}"/env.d 50shelx || die
}

