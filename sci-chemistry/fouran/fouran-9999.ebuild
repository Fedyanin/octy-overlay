# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
EAPI=8

inherit git-r3

DESCRIPTION="Residual density analysis"
HOMEPAGE="https://gitlab.com/Fedyanin/fouran"
EGIT_REPO_URI="https://gitlab.com/Fedyanin/fouran.git"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_install()
{
	dobin fouran
}
