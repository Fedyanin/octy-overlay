# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

DESCRIPTION="Perl scripts to work with charmm, statistics etc."
HOMEPAGE="http://www.octy.org/"
SRC_URI="www.octy.org/${P}.tar.bz2"

# fixme change to gpl-3 after adding to files
LICENSE="freeware"

SLOT="0"
KEYWORDS="x86 ~amd64"
IUSE=""

RDEPEND=""

src_install()
{
	exeinto /usr/bin
	doexe o_uiparser
	doexe o_stat
}
