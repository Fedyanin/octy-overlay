# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI="8"

inherit unpacker xdg-utils

DESCRIPTION="Crystal Structure Analysis with Hirshfeld Surfaces"
HOMEPAGE="http://hirshfeldsurface.net"

SRC_URI="https://releases.crystalexplorer.net/CrystalExplorer-21.5-ubuntu-20.04.deb"

LICENSE="CrystalExplorer"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="nomirror"

XDG_ECLASS_MIMEINFOFILES="${FILESDIR}/x-cxp.mime"

DEPEND="${RDEPEND}"

S=${WORKDIR}

src_prepare() {
	sed -i -e 's/Exec=CrystalExplorer/Exec=CrystalExplorer --open %F\nCategories=Science/' \
		-e 's/Icon=crystalexplorer/Icon=\/usr\/share\/pixmaps\/crystalexplorer.png\nMimeType=chemical\/x-cxp/' \
		-e '/Version/d' ${S}/usr/share/applications/crystalexplorer.desktop
	mkdir -p ${S}/usr/share/mime/packages
	cp "${FILESDIR}/x-cxp.xml" ${S}/usr/share/mime/packages
	eapply_user
}


src_install() {
	mv ${S}/usr/bin/CrystalExplorer ${S}/usr/bin/CrystalExplorer.run
	cp -R ${S}/* ${D}
	exeinto /usr/bin
	doexe ${FILESDIR}/CrystalExplorer
	insinto /usr/share/mime/chemical
}

pkg_postinst()
{
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
