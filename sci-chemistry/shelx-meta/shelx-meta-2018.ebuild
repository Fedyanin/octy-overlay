# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Metapackage for shelx installation"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="amd64"
IUSE="+ciftab +shelxd +shelxt +shelxs +shredcif +shelxl +shelxtl"

DEPEND="shelxd? ( sci-chemistry/shelxd-bin )
        shelxl? ( sci-chemistry/shelxl-bin )
		shelxs? ( sci-chemistry/shelxs-bin )
		shelxt? ( sci-chemistry/shelxt-bin )
		shredcif? ( sci-chemistry/shredcif-bin )
		shelxtl? ( sci-chemistry/shelxtl-bin )
		ciftab? ( sci-chemistry/ciftab-bin )"
RDEPEND="${DEPEND}"
