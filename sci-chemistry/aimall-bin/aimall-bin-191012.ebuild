# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
EAPI="2"

YY=${PV:0:2}
MM=${PV:2:2}
DD=${PV:4:2}

DISTFTP="ftp://10.5.0.60/distfiles"
RESTRICT="nomirror"

SRC_URI="
          x86? ( $DISTFTP/aimall_${YY}_${MM}_${DD}_linux_32bit.tar.gz )
		  amd64? ( $DISTFTP/aimall_${YY}_${MM}_${DD}_linux_64bit.tar.gz )
		 "
VERSION="$YY.$MM.$DD"

DESCRIPTION="Computational Chemistry Using QTAIM"
HOMEPAGE="http://aim.tkgristmill.com/index.html"


LICENSE="AIMALL"
SLOT="0"
KEYWORDS="x86 amd64"

RDEPEND="x11-terms/rxvt-unicode x11-libs/libXext"
DEPEND="${RDEPEND}"


pkg_nofetch() {
	elog "Please register at http://aim.tkgristmill.com "
	elog "then download binaries from the site, and place it to ${DISTDIR}"
}

S=${WORKDIR}/AIMAll

src_install() {
	ddir="$D/opt/aimall"
	bdir="$D/opt/bin"
	mkdir -p $ddir
	mkdir -p $bdir
	cp -R ${S}/* ${ddir}
	cd ${ddir}
	for i in "aimint" "aimqb" "aimstudio" "aimsum" "aimutil" "aimext"; do
	    chmod 755 "bin/$i.exe"
		rm $i.ish
		echo "#!/bin/sh" > $i
		echo "export LD_LIBRARY_PATH=\"/opt/aimall/lib\":$LD_LIBRARY_PATH" >> $i
		if [ $i != "aimext" ]; then
			echo "/opt/aimall/bin/$i.exe \$*" >> $i
		else
			echo "if [ -n \"\$DISPLAY\" ]; then" >> $i
			echo "  urxvt -title \"AIMExt (Version $VERSION)\" -e /opt/aimall/bin/$i.exe \$*" >> $i
			echo "else" >> $i
			echo "/opt/aimall/bin/$i.exe \$*" >> $i
			echo "fi" >> $i
		fi
		chmod 755 $i
		ln -s /opt/aimall/$i $bdir/$i
	done
}

