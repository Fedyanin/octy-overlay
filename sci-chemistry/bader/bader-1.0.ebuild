# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Bader charge density analysis program"
HOMEPAGE="http://theory.cm.utexas.edu/henkelman/code/bader"
SRC_URI="http://theory.cm.utexas.edu/henkelman/code/bader/download/bader.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+ifort"

DEPEND="ifort? ( dev-lang/ifc )"
RDEPEND="${DEPEND}"

S=${WORKDIR}/${PN}

src_compile()
{
	if use ifort; then
	    sed -i -e 's/FFLAGS =.*/FFLAGS = -O2 -xHost/g' makefile.lnx_ifort
	else
	    sed -i -e 's/FC =.*/FC = gfortran/g' makefile.lnx_ifort
	fi
	make -f makefile.lnx_ifort
}

src_install()
{
	dobin bader
}
