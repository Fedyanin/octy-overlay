# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A program to solve, refine and finish small-molecule crystal structures "
HOMEPAGE="http://www.olexsys.org/"
SRC_URI="olex2-linux64.zip"

LICENSE="OLEX2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S=${WORKDIR}

src_install()
{
	mkdir -p ${D}/opt
	cp -r ${S}/olex2 ${D}/opt
	sed -i -e's/bin_dir=.*/bin_dir="/opt/olex2"' ${D}/opt/olex2/start
	dosym ${D}/opt/olex2/start /opt/bin/olex2
}
