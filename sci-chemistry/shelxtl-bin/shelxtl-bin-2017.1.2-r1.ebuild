# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=7
#this ebuild uses new files from SHELX site and only few files from BRUKER

inherit rpm

RV="${PV%.*}-${PV##*.}"
RN=${PN%-bin}

SRC_FILE_ADD1="xp-5.1.exe.gz"

BIN_FEATURES="cell_now sadabs twinabs xl97 xprep xpx"

SHELX_DIR=/opt/shelx/bin

DESCRIPTION="SHELXTL program suite"
HOMEPAGE="http://www.brukersupport.com/SoftwarePreview.aspx"
SRC_URI="${RN}-${RV}.i386.rpm xp_wine? ( $SRC_FILE_ADD1 )"

SLOT="0"
KEYWORDS="~amd64"
RESTRICT="fetch"

IUSE="+xp_wine"
for i in $BIN_FEATURES; do
   IUSE="${IUSE} +$i"
done

RDEPEND="x11-terms/rxvt-unicode
         xp_wine? ( app-eselect/eselect-wine  )"

S=${WORKDIR}

src_install() {
	ADD_EXE=
	ADD_FILES=

	
	exeinto /opt/shelx/bin
	for i in $BIN_FEATURES; do
	    if use $i; then 
		    if [ "$i" == "xprep" ]; then
				continue
			fi
			doexe ${S}/usr/local/bin/$i
			dosym "${SHELX_DIR}/$i" "/opt/bin/$i"
		fi
	done
	
	if use xp_wine; then
	    echo -ne "#!/bin/sh\n\nwine /opt/shelx/bin/xp-5.1.exe \$@ 2>/dev/null &\n" > ${S}/usr/local/bin/xp
		insinto /opt/shelx/bin
		doins "${S}/xp-5.1.exe"
		doexe ${S}/usr/local/bin/xp
		dosym "${SHELX_DIR}/xp" "/opt/bin/xp"
	fi

	if use xprep; then
	    mv ${S}/usr/local/bin/xprep ${S}/usr/local/bin/xprep.x 
		echo -ne "#!/bin/sh\n\nurxvt -bg white -cd \`pwd\` -e /opt/shelx/bin/xprep.x \$@ &\n" >${S}/usr/local/bin/xprep
		doexe ${S}/usr/local/bin/xprep
		doexe ${S}/usr/local/bin/xprep.x
		dosym "${SHELX_DIR}/xprep" "/opt/bin/xprep"
	fi

	

	insinto opt/shelx/bin
	for i in xcif.ang xcif.def xcif.ger xcif.met xcif.rta xcif.rtm; do
		doins $S/usr/local/bin/$i;
	done

	insinto opt/shelx
	doins ${S}/usr/local/bruker/sxtl.ini

	cp -R $S/usr/local/bruker/help ${D}/opt/shelx/bruker

	echo "SXTL=\"/opt/shelx/bruker/sxtl.ini\"" > "${T}"/env.d
	newenvd "${T}"/env.d 50shelx || die
}

