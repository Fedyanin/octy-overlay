# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=8

inherit rpm

RN="Marvin_linux"

DESCRIPTION="Intuitive applications and API for chemical sketching, visualization and data exploration"
HOMEPAGE="https://www.chemaxon.com/products/marvin"
SRC_URI="${RN}_${PV}.rpm"
RESTRICT="nofetch"

LICENSE="CHEMAXON"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="app-arch/rpm2targz"
RDEPEND="${DEPEND} >=virtual/jre-1.8.0"

S="$WORKDIR"

pkg_nofetch() {
	elog "Please register at "
	elog "https://www.chemaxon.com/forum/profile.php?mode=register&agreed=true"
	elog "then download ${RN}_${PV}.rpm from https://www.chemaxon.com/download/marvin-suite/"
	elog "and place it to ${DISTDIR}"
}

src_unpack() {
    rpm_unpack "$A"
}

src_install() {
	ddir="$D"
	bdir="$D/opt/bin"
	mkdir -p $ddir $bdir
	cp -r ${S}/* $ddir

	doicon ${S}/opt/chemaxon/marvinsuite/.install4j/MarvinSketch.png
	doicon ${S}/opt/chemaxon/marvinsuite/.install4j/MarvinView.png

	dosym /opt/chemaxon/marvinsuite/bin/msketch /opt/bin/msketch
	dosym /opt/chemaxon/marvinsuite/bin/msketch /opt/bin/mview
	make_desktop_entry msketch MarvinSketch MarvinSketch Science || die "make_desktop_entry failed"
	make_desktop_entry mview MarvinView MarvinView Science || die "make_desktop_entry failed"
}
