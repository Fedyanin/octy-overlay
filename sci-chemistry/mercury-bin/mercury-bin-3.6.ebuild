# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI="4"

inherit eutils fdo-mime

DESCRIPTION="Crystal Structure Visualisation and Exploration Made Easy"
HOMEPAGE="http://www.ccdc.cam.ac.uk/free_services/mercury/"

#FIXME: quick fix for 3.5.1 download path. Uncomment if newer version use sane path
PMV=${PV%.*}
SRC_URI="https://downloads.ccdc.cam.ac.uk/Mercury/$PV/mercurystandalone-${PV}-linux-installer.run"

LICENSE="CCDC"
SLOT="0"
KEYWORDS="x86 amd64"
RESTRICT="nomirror"

DEPEND="${RDEPEND}"
S=${WORKDIR}

pkg_setup() {
	use x86 && IUSE="+system-qt"
}


src_unpack() {
# use an empty RPM trick to fake the installer
	cd ${S}
	cp ${DISTDIR}/${A} .
	echo -ne "#!/bin/sh\nexit\n" >> "rpm"
	chmod u+x ${A} "rpm"
	mkdir opt
	PATH=.:$PATH ${A} --mode unattended --unattendedmodeui none --prefix ${S}/opt/mercury
	if use system-qt; then
		rm -rf opt/mercury/c_linux/lib/qt
	fi
	sed -i -e 's/check_for_updates = 1/check_for_updates = 0/' opt/mercury/update_software.ini
	chmod 644 opt/mercury/icons/*
}

src_compile() {
	cd ${S}
	echo -ne "#!/bin/sh\n/opt/${PN}/bin/mercury \$@ &>/dev/null &\n" > mercury
}


src_install() {
	dodir /opt/${PN}
	cp -R ${S}/opt/mercury/* ${D}/opt/${PN}
	dodir /opt/bin
	exeinto /opt/bin
	doexe mercury
	domenu "${FILESDIR}"/mercury.desktop || die "domenu failed"
}

pkg_postinst() {
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
}

pkg_postrm() {
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
}
