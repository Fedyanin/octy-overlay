# 
# 
# 
EAPI="8"


DESCRIPTION="Visualization index based on the density and its derivatives"
HOMEPAGE="http://www.lct.jussieu.fr/pagesperso/contrera/index-nci.html"
SRC_URI="http://www.lct.jussieu.fr/pagesperso/contrera/nciplot-${PV}.tar.gz -> nciplot-${PV}.tar"

LICENSE="GPL"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="-ifort -testcases"

pkg_setup() {
	if use ifort; then
	    ewarn "You are trying to compile ${PN} with ifort. Check if it's avaliable."
		if [ "`ifort -v > /dev/null 2>/dev/null; echo $?`" == "0" ]; then
		    ewarn "OK!"
		else
		    die "ifort is not avaliable on you system. Please drop this use flag"
		fi
	fi
}

src_prepare() {
	if use ifort; then
	    cd ${S}/src
		sed -i -e '/^## The Intel/,/^#endif/s/^#//' -e '/# The GNU/,/^endif/d' Makefile.inc
		sed -i -e '/LDFLAGS/s/$/ -static-intel/' Makefile.inc
	fi
	eapply_user
}

src_compile() {
	cd ${S}/src
	make
}

src_install() {
	dobin src/${PN} || die

	insinto /usr/share/${PN}/dat
	for i in dat/* ; do
		doins $i
	done

	insinto /usr/share/${PN}/doc
	doins doc/nciplot-manual.pdf

	if use testcases; then
	    cp -r test-cases  ${D}/usr/share/${PN}
	fi
}
