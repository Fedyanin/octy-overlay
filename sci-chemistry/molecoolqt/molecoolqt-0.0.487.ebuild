# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit qt4-r2


DESCRIPTION="Molecule viewer for charge-density research"
HOMEPAGE="http://ewald.ac.chemie.uni-goettingen.de/___mole_cool_qt__"
SRC_URI="molecoolqt-${PV}.tar.bz2"
RESTRICT="nofetch"

LICENSE="MoleCoolQt"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="sci-libs/fftw:3.0 dev-qt/qtcore:4 dev-qt/qtopengl:4 dev-qt/qtgui:4"
RDEPEND="${DEPEND}"

pkg_nofetch() {
	elog "Please download 'molecoolqt-${PV}.tar.bz2' from $HOMEPAGE"
	elog "and place it to ${DISTDIR}"
}

src_configure() {
	eqmake4 *.pro PREFIX=/usr
	sed -i -e 's/^ *//g' -e '/Categories/s/$/;/' *.desktop
}
