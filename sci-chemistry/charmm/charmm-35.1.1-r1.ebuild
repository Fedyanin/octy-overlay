# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

PROGVER="c35b1r1"

DESCRIPTION="Chemistry at HARvard Macromolecular Mechanics"
HOMEPAGE="http://www.charmm.org/"
SRC_URI="http://www.charmm.org/jast-to-give-pname/charmm-${PROGVER}.tar.lzma"
RESTRICT="fetch"

EAPI="2"

LICENSE="proprietary"
SLOT="0"
KEYWORDS="x86"

DEPEND="sys-devel/gcc[fortran] app-arch/p7zip app-shells/tcsh"
RDEPEND=""
IUSE="X"

S="${WORKDIR}/${PROGVER}"

src_unpack() {
	unpack ${A}
}

src_prepare() {
	epatch "${FILESDIR}"/charmm-parallel_pme-0.1.patch
	epatch "${FILESDIR}"/charmm-ew3dc_correction-0.1.patch
}

src_compile() {
	if [ -z "${MEMORY_SIZE}" ];
		then MEMORY_SIZE="xlarge";
		ewarn "Default charmm memory size (xlarge) is used. If you want the other setting"
		ewarn "(from huge, xxlarge, xlarge, large, medium, small, xsmall, reduce)"
		ewarn "to be used, specify MEMORY_SIZE variable e.g. in your 'make.conf' file."
	fi

	XSET="X"
	use X || XSET="NODISP"

	tcsh install.com gnu ${MEMORY_SIZE} ${XSET}

	if [ ! -f "${S}/exec/gnu/charmm" ] ; then
		eerror "Compile filed, check"
		eerror "/mnt/tmpfs/portage/sci-chemistry/charmm-35.1-r1/work/c35b1r1/build/gnu/gnu.log"
		die
	fi
	# make docs
	cd support/htmldoc
	tcsh doc2html.com ../../ %{$PROGVER} $(expr substr ${PROGVER} 2 2)
}


src_install() {
	rm "${S}/toppar/toppar_history/c32b2/silicates/code/patchfind.x"
	rm "${S}/toppar/toppar_history/c33b2/silicates/code/patchfind.x"
	rm "${S}/toppar/toppar_history/c34b2/silicates/code/patchfind.x"
	dodir  /opt/${PN}
	cp -R "${S}/doc" "${D}/opt/${PN}"
	cp -R "${S}/html" "${D}/opt/${PN}"
	cp -R "${S}/support" "${D}/opt/${PN}"
	cp -R "${S}/test" "${D}/opt/${PN}"
	cp -R "${S}/toppar" "${D}/opt/${PN}"
	exeinto /opt/${PN}
	doexe  ${S}/exec/gnu/charmm
	dosym /opt/${PN}/charmm /opt/bin/charmm
}
