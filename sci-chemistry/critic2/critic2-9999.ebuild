# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

CMAKE_MAKEFILE_GENERATOR=emake
inherit git-r3 cmake

DESCRIPTION="Program for the topological anaylisis of real-space scalar fields in periodic systems"
HOMEPAGE="http://schooner.chem.dal.ca/wiki/Critic2"
EGIT_REPO_URI="https://github.com/aoterodelaroza/critic2.git"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

