# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=7

DESCRIPTION="PLATON check.def file with latest definitions of rules"
HOMEPAGE="http://www.platonsoft.nl/xraysoft/unix/platon/"
SRC_URI="http://www.platonsoft.nl/xraysoft/unix/platon/check.def"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="${DEPEND}"

S=${WORKDIR}

pkg_nofetch() {
	elog "If there is a digest mismatch, please update the ebuild ---"
	elog "a version bump is probably required."
}

src_unpack() {
	true
}

src_install() {
	insinto /usr/$(get_libdir)/platon
	doins ${DISTDIR}/check.def || die
}
