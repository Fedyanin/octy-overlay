# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PN_WTBIN="${PN%-bin}"
CMD=${PN_WTBIN#shel}

ARCH="x86_64"

FILE="${PN_WTBIN}-${ARCH}-${PV}"

SHELX_DIR=/opt/shelx/bin

DESCRIPTION="shelxl binary distribution"
HOMEPAGE="http://shelx.uni-goettingen.de"
SRC_URI="${FILE}.bz2"

SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"


src_unpack()
{
	unpack ${A}
	mv "$FILE" "$CMD"
}

S="${WORKDIR}"

src_install()
{
	exeinto ${SHELX_DIR}
	doexe "$CMD"
	dosym "${SHELX_DIR}/$CMD" "/opt/bin/$CMD"
}
