# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MOD_TITLE="Insomnia"

#inherit eutils games games-mod
inherit eutils games

DESCRIPTION="Classic Quake 1 mod with huge levels"
HOMEPAGE="http://retroquake.planetquake.gamespy.com/blog/?p=72"
#SRC_URI="${SRC_ADDICT}/${MOD_FILENAME}
#	${SRC_TERMINUS}/${MOD_FILENAME}
#	lights? ( http://www.kgbsyndicate.com/romi/czg07.rar )"
SRC_URI="http://www.quaketerminus.com/hosted/happymaps/files/q/czg07.zip"

RESTRICT="nomirror"

MOD_DEPEND="app-arch/unrar"
SLOT="0"
KEYWORDS="x86"

src_unpack() {
	mkdir -p ${S}
	cd "${S}"
	unpack "czg07.zip"
}

src_install() {
	insinto /usr/share/games/quake1/insomnia
	doins *.*
}
