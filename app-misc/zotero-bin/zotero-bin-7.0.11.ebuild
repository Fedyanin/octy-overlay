# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=8

inherit desktop xdg

DESCRIPTION="Zotero is a tool to collect, organize, cite, and share your research sources"
HOMEPAGE="http://www.zotero.org/"
SRC_URI=" 
   x86? ( https://download.zotero.org/client/release/${PV}/Zotero-${PV}_linux-i686.tar.bz2 )
   amd64? ( https://download.zotero.org/client/release/${PV}/Zotero-${PV}_linux-x86_64.tar.bz2 )
   "
RESTRICT="nomirror"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"

pkg_setup() {
	if use x86; then
		S="${WORKDIR}/Zotero_linux-i686"
	elif use amd64; then
		S="${WORKDIR}/Zotero_linux-x86_64";
	fi
}

src_install() {
	mkdir -p $D/opt/zotero
	cp -R $S/* $D/opt/zotero
	sed -i -e "s@^Exec=.*@Exec=/opt/zotero/zotero@" $S/zotero.desktop
	sed -i -e "s@^Icon=.*@Icon=/opt/zotero/icons/icon128.png@" $S/zotero.desktop
	dosym /opt/zotero/zotero opt/bin/zotero
	insinto /usr/share/applications
	doins $S/zotero.desktop
}

pkg_postinst() {
    xdg_desktop_database_update
}

pkg_postrm() {
    xdg_desktop_database_update
}
