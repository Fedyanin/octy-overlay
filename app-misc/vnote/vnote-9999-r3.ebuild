# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
EAPI=7
CMAKE_ECLASS=cmake

inherit git-r3 qmake-utils

DESCRIPTION="A note-taking application that knows programmers and Markdown better!"
HOMEPAGE="https://tamlok.github.io/vnote"
EGIT_REPO_URI="https://github.com/tamlok/vnote.git"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="amd64"
IUSE=""

DEPEND=""
RDEPEND="
dev-qt/qtcore
dev-qt/qtdeclarative
dev-qt/qtgui
dev-qt/qtnetwork
dev-qt/qtprintsupport
dev-qt/qtsvg
dev-qt/qtwebchannel
dev-qt/qtwebengine
dev-qt/qtwidgets
"

src_configure() {
	eqmake5 vnote.pro
}


src_install() {
	emake install INSTALL_ROOT="${D}" LIBDIR="${D}/lib64"
	mv ${D}/usr/lib ${D}/usr/lib64
}
