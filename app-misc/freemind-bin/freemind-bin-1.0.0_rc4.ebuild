# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils

PVN=${PV%_rc*}
PRN=${PV##*_rc}

DESCRIPTION="Mind-mapping software written in Java"
HOMEPAGE="http://freemind.sourceforge.net"
SRC_URI="http://sunet.dl.sourceforge.net/project/freemind/freemind-unstable/${PVN}_RC${PRN}/freemind-bin-max-${PVN}_RC_${PRN}.zip"
RESTRICT="nomirror"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86"

RDEPEND=">=virtual/jre-1.5.0 !app-misc/freemind"
DEPEND="${RDEPEND}"
IUSE=""

S="${WORKDIR}/${PN}"

src_unpack() {
	mkdir ${WORKDIR}/${PN}
	cd ${S}
	unpack ${A}
	unzip -qq lib/freemind.jar images/76812-freemind_v0.4.png
	mv images/76812-freemind_v0.4.png images/freemind_bin.png
	rm freemind.bat FreeMind.exe
	mv freemind.sh freemind
}

src_install() {
	doicon images/freemind_bin.png
	rm -rf ${S}/images
	dodir  /opt/${PN}
	cp -R ${S}/* ${D}/opt/${PN}
	exeinto /opt/${PN}
	doexe freemind
	dosed "s:\#\!\/bin\/sh:&\nFREEMIND_BASE_DIR=/opt/${PN}:g" /opt/${PN}/freemind

	dosym /opt/${PN}/freemind /opt/bin/freemind
	make_desktop_entry freemind Freemind freemind_bin Office || die "make_desktop_entry failed"
}
