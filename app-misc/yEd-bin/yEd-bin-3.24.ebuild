# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI="7"

inherit desktop

XPN=${PN%-bin}
XPN=${XPN,,}

DESCRIPTION="yEd is a powerful desktop application to quickly and effectively generate high-quality diagrams."
HOMEPAGE="http://www.yworks.com/en/products_yed_about.html"
SRC_URI="http://www.yworks.com/products/yed/demo/yEd-${PV}.zip"
RESTRICT="fetch"


LICENSE="yEd"
SLOT="0"
KEYWORDS="~x86 ~amd64"

RDEPEND="app-arch/unzip >=virtual/jre-1.8.0"
DEPEND="${RDEPEND}"
IUSE=""
S="${WORKDIR}/${XPN}-${PV}"

src_install() {
	cd $S
	echo -ne "#!/bin/sh\njava -jar /opt/${PN}/yed.jar \$* &\n" > ${XPN}
	insinto /opt/${PN}
	doins yed.jar
	doins license.html
	insinto /opt/${PN}/lib
	for i in $S/lib/*; do
	    doins $i
	done
	doicon icons/yed32.png
	exeinto /opt/${PN}
	doexe ${XPN}

	dosym /opt/${PN}/${XPN} /opt/bin/${XPN}
	make_desktop_entry yed yEd yed32 Office || die "make_desktop_entry failed"
}
