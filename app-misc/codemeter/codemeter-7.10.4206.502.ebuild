# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
EAPI=8

inherit unpacker

DESCRIPTION="codemeter"
HOMEPAGE="NULL"
SRC_URI="codemeter_${PV}_amd64.deb"

RESTRICT="nofetch"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
S=${WORKDIR}

src_install() {
		cp -R ${S}/* ${D}
}
