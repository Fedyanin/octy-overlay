# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-arch/torrentzip/torrentzip-0.2-r1.ebuild,v 1.3 2009/10/12 16:51:50 halcy0n Exp $

inherit eutils

DESCRIPTION="The free translation memory tool"
HOMEPAGE="http://www.omegat.org/"

IPN="OmegaT"
IPD="OmegaT-bin"
IPV=${PVR%_p*}
IPR=${PVR#*_p}

SRC_URI="mirror://sourceforge/omegat/${IPN}_${IPV}_${IPR}_Without_JRE.zip"

RDEPEND=">=virtual/jre-1.8.0"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
DEPEND="app-arch/unzip"

LANGS="ar ca cy de en es fr hu it nl pt_BR sh sl sv zh_CN be cs da el eo eu gl ja pl ru sk sq tr zh_TW"
for i in ${LANGS}; do
	IUSE="${IUSE} linguas_${i}";
done


S=${WORKDIR}

src_install() {
	dodir /opt/${IPD}
	for dir in images lib plugins scripts; do
		cp -R "${dir}" "${D}/opt/${IPD}"
	done

	insinto "/opt/${IPD}/docs"
	doins docs/index.html

	insinto "/opt/${IPD}"
	for i in ${LANGS}; do
	    if use "linguas_${i}"; then
			cp -R "docs/${i}" "${D}/opt/${IPD}/docs"
			if [ "$i" == "en" ]; then
				doins "readme.txt"
			else
				doins "readme_${i}.txt"
			fi
		fi
	done

	if use x86; then
	   cp "native/libhunspell-linux32.so" "${D}/opt/${IPD}/native"
	elif use amd64; then
	   cp "native/libhunspell-linux64.so" "${D}/opt/${IPD}/native"
    fi

	doins OmegaT.jar
	doins OmegaT-license.txt
	doins doc-license.txt
	doins changes.txt

	exeinto /opt/${IPD}
	#sed -i -e "s,OmegaT,/opt/${IPD}/${IPN}," OmegaT
	doexe OmegaT

	dosym "/opt/${IPD}/${IPN}" "/opt/bin/${IPN}"
	make_desktop_entry OmegaT OmegaT /opt/${IPD}/images/OmegaT.png Office || die "make_desktop_entry failed"
}
