# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=8
inherit desktop

DESCRIPTION="A free cross-platform English thesaurus"
HOMEPAGE="http://artha.sourceforge.net/wiki/index.php/Home"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="enchant libnotify"

DEPEND=">=app-dicts/wordnet-3
	>=x11-libs/gtk+-2.12
	>=dev-libs/glib-2.14
	>=dev-libs/dbus-glib-0.70"
RDEPEND="${DEPEND}
	enchant? ( app-text/enchant )
	libnotify? ( >=x11-libs/libnotify-0.4.1 )"

#src_prepare() {
#	eapply "${FILESDIR}"/${PV}-config.patch
#	eapply_user
#	eautoreconf
#}

src_install() {
	make DESTDIR=${D} install || die "make install failed"
	make_desktop_entry /opt/bin/artha Artha \
			"/usr/share/pixmaps/artha.png" Office \
			|| die "make_desktop_entry failed"
}

